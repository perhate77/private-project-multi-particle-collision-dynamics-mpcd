**This is a serial MPCD code of translocation of a polymer through nano pore in a complex fluid, written in C.**


   
 	 

 *	 Compile code using: 
 *	 	 gcc filename.c -lm

      



 *	 Run :
 *		 nohup ./a.out &
 			

![VelCorr](/uploads/ebfc831578cab5ee8a0d1cdde29d552a/VelCorr.png)
     
 Written By::<br/>
       **Rajneesh Kumar**
 *	Theoretical Sciences Unit, Jawaharlal Nehru Centre for Advanced Scientific Research,
 *	Bengaluru 560064, India.
 *	Email: rajneesh[at]jncasr.ac.in
 *	27 Dec, 2020

